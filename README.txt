Standalone "universal binary" Atlassian Remote App plugin

This is a "unversal binary" remote app that demonstrates a single artifact that can be deployed
standalone outside an Atlassian application on a hosting service such as Heroku, or for behind-the-firewall
instances, install as a plugin.

To get started, build the app:
  mvn package
  
To install the app, upload the plugin to a target instance such as https://remoteapps.jira.com

Instructions on running standalone to come...
