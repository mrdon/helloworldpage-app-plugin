package servlets;

import com.atlassian.plugin.remotable.kit.servlet.AbstractPageServlet;

import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;

/**
 *
 */
@Named
public class HelloWorldServlet extends AbstractPageServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        render(req, resp, Collections.<String, Object>singletonMap("date", new Date().toString()));

    }
}
